@extends('layouts.app')

@section('title', 'Ошибка оплаты')

@section('content')
    <h1>Извините, при оплате произошла ошибка.</h1>

    @if(isset($data))
	    <div>
	    	<b>ID заказа:</b> {{ $data['order']['order_id'] ?? '' }}
	    </div>
	    <br>
	    <div>
	    	<b>Статус заказа:</b> {{ $data['order']['status'] ?? '' }}
	    </div>
	    <br>
	    <div>
	    	<b>Статус транзакции:</b> {{ $data['transaction']['status'] ?? '' }}
	    </div>
	    <br>
	    <div>
	    	<b>Токен формы оплаты:</b> {{ $data['pay_form']['token'] ?? '' }}
	    </div>
	    <br>
	    <div>
	    	<b>Код ошибки:</b> {{ $data['error']['code'] ?? '' }}
	    </div>
	    <br>
	    <div>
	    	<b>Ошибка:</b> {{ $data['error']['recommended_message_for_user'] ?? '' }}
	    </div>
	@else
		<h3>К сожалению детали оплаты недоступны.</h3>
	@endif
@endsection