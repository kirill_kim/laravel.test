@extends('layouts.app')

@section('title', 'Оплата прошла успешно')

@section('content')
    <h1>Спасибо, оплата прошла успешно!</h1>

    @if(isset($data))
	    <div>
	    	<b>ID заказа:</b> {{ $data['order']['order_id'] ?? '' }}
	    </div>
	    <br>
	    <div>
	    	<b>Статус заказа:</b> {{ $data['order']['status'] ?? '' }}
	    </div>
	    <br>
	    <div>
	    	<b>Токен формы оплаты:</b> {{ $data['pay_form']['token'] ?? '' }}
	    </div>
	@else
		<h3>К сожалению детали оплаты недоступны.</h3>
	@endif
@endsection