<!DOCTYPE html>
<html>
<head>
	<title>@yield('title', 'Default')</title>
	<meta charset="utf-8">
	<style>
		body {
			margin: 0;
			padding: 0;
			font-size: 16px;
		}

		header {
			background: #282923;
			padding: 30px 0;
			margin-bottom: 50px;
		}

		footer {
			background: #282923;
			padding: 10px 0;
			margin-top: 50px;
		}

		header h2, footer h2 {
			text-align: center;
			color: #fff;
		}

		.content {
			margin: 0 50px;
		}
	</style>
</head>
<body>
	<header>
		<h2>Шапка сайта</h2>
	</header>

	<section class="content">
		@section('content')
			<h1>Добро пожаловать!</h1>
        @show
	</section>

	<footer>
		<h2>Подвал сайта</h2>
	</footer>
</body>
</html>