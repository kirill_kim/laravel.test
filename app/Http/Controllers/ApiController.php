<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Services\CustomValidationService;

class ApiController extends Controller
{
	private $customValidationService;

    public function __construct(CustomValidationService $customValidationService)
    {
        $this->customValidationService = $customValidationService;
    }

    public function callback(Request $request)
    {
    	$json = json_decode($request->json, true);

	    // Validation
	    if (!is_array($json) || $this->customValidationService->callbackInputIsFail($json)) {
	    	return response('Data is not valid', 400);
	    }

	    // Get order, return error if order not found
    	$orderId = $json['order']['order_id'];
    	if (!$order = Order::whereOrderId($orderId)->first()) {
    		return response('Order not found', 404);
    	}

    	// Update status
    	$order->status = $json['order']['status'];
    	$order->save();

    	if (isset($json['error'])) {
    		// Оплата неудачная
    			return redirect()->route('callback_sorry', ['json' => $request->json]);
    	}

    	return redirect()->route('callback_thanks', ['json' => $request->json]);
    }
}
