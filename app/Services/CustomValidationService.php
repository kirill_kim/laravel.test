<?php

namespace App\Services;

use Illuminate\Support\Facades\Validator;

class CustomValidationService
{
    public function callbackInputIsFail($array)
    {
        $rules = [
	    	'pay_form' => 'required|array',
	    	'pay_form.token' => 'required|string',

	    	'transactions' => 'array',
	    	
	    	'error' => 'array',

	        'order' => 'required|array',
	        'order.order_id' => 'required|string',
	        'order.status' => 'required|string',

	        'transaction' => 'required|array',
	        'transaction.id' => 'required|string',
	        'transaction.operation' => 'required|string',
	        'transaction.status' => 'required|string'
	    ];

	    return Validator::make($array, $rules)->fails();
    }
}