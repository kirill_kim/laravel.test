<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts/app');
});

Route::get('/sorry', function (Request $request) {
    return view('callback/sorry', ['data' => json_decode($request->json, true)]);
})->name('callback_sorry');

Route::get('/thank-you', function (Request $request) {
    return view('callback/thanks', ['data' => json_decode($request->json, true)]);
})->name('callback_thanks');
